waitUntil { !isNil {player} };
waitUntil { player == player };

player createDiaryRecord ["Diary", ["CREDITS", "Mission by |TG| Will for tacticalgamer.com. Edit or update as you wish, just keep the credits:<br/><br/>aeroson - group manager script.<br/><br/>|TG|Toptonic-Butler - name tag script.<br/><br/>|TG| Unkl - TGBanner graphics.<br/><br/>Tonic - View Distance script<br/><br/>Bangabob - Enemy Occupation System."]];
player createDiaryRecord ["Diary", ["MEDICAL", "Currently respawn only. As you capture objectives new respawn points will open up."]];
player createDiaryRecord ["Diary", ["ASSETS", "Assets will become available as the infrastructure to support them is captured. Vehicles will respawn at the same location once they are destroyed.<br><br>Fast roping is available from the helicopters - the pilot must bring the chopper into a hover and use the scroll wheel to 'Deploy Ropes'.  Players can then exit the chopper one by one using their scroll wheel menu.<br><br>"]];
player createDiaryRecord ["Diary", ["HINTS", "Name Tag script is active. Press U to see the names of your squadmates. Leaders are able to see other leaders as well."]];
player createDiaryRecord ["Diary", ["MISSION", "Insert at the <marker name='respawn_west'>beachhead</marker>, and then following the objectives laid out by HQ in order to capture the island."]];
player createDiaryRecord ["Diary", ["SITUATION", "You have landed with a small number of men in the North East of Altis. Follow HQ's orders and capture the island. As you gain more ground HQ will be able to deploy more resources to the theatre."]];
