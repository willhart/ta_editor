﻿<Window x:Class="AnvilEditor.MainWindow"
        xmlns="http://schemas.microsoft.com/winfx/2006/xaml/presentation"
        xmlns:x="http://schemas.microsoft.com/winfx/2006/xaml"
        xmlns:xctk="http://schemas.xceed.com/wpf/xaml/toolkit"
        xmlns:anvil="clr-namespace:AnvilEditor"
        Title="Anvil Editor v3.2" WindowStartupLocation="CenterScreen" KeyDown="WindowKeyDown" KeyUp="WindowKeyUp" Width="1280" Height="720">

    <Window.CommandBindings>
        <CommandBinding Command="ApplicationCommands.New" CanExecute="CommandAlwaysExecutable" Executed="NewButtonClick" />
        <CommandBinding Command="ApplicationCommands.Open" CanExecute="CommandAlwaysExecutable" Executed="LoadMissionClick" />
        <CommandBinding Command="ApplicationCommands.Save" CanExecute="CommandAlwaysExecutable" Executed="SaveMission" />
        <CommandBinding Command="ApplicationCommands.SaveAs" CanExecute="CommandAlwaysExecutable" Executed="ExportMissionFiles" />
        <CommandBinding Command="ApplicationCommands.Find" CanExecute="CommandAlwaysExecutable" Executed="FindObjective" />
        <CommandBinding Command="ApplicationCommands.Delete" CanExecute="CommandWithSelectedObjective" Executed="DeleteSelectedObjective" />

        <CommandBinding Command="{x:Static anvil:MainWindow.EnterCreateModeCommand}" CanExecute="CommandAlwaysExecutable" Executed="CreateModeButtonChecked" />
        <CommandBinding Command="{x:Static anvil:MainWindow.EnterEditModeCommand}" CanExecute="CommandAlwaysExecutable" Executed="EditModeButtonChecked" />
        <CommandBinding Command="{x:Static anvil:MainWindow.EnterZoomModeCommand}" CanExecute="CommandAlwaysExecutable" Executed="ZoomModeButtonChecked" />
        <CommandBinding Command="{x:Static anvil:MainWindow.EnterRespawnModeCommand}" CanExecute="CommandAlwaysExecutable" Executed="EnterRespawnMode" />
        <CommandBinding Command="{x:Static anvil:MainWindow.EnterAmbientModeCommand}" CanExecute="CommandAlwaysExecutable" Executed="EnterAmbientMode" />
        <CommandBinding Command="{x:Static anvil:MainWindow.ShowSQMEditorCommand}" CanExecute="CommandAlwaysExecutable" Executed="ShowSQMEditor" />
        <CommandBinding Command="{x:Static anvil:MainWindow.RefreshMissionFromSqmCommand}" CanExecute="CommandWithLoadedPath" Executed="RefreshMissionFromSqm" />
        <CommandBinding Command="{x:Static anvil:MainWindow.CheckForUpdatesCommand}" CanExecute="CommandAlwaysExecutable" Executed="CheckForUpdates" />
        <CommandBinding Command="{x:Static anvil:MainWindow.PerformCleanBuildCommand}" CanExecute="CommandWithLoadedPath" Executed="PerformCleanBuild" />
        <CommandBinding Command="{x:Static anvil:MainWindow.ManualFrameworkUpdateCommand}" CanExecute="CommandAlwaysExecutable" Executed="ManualFrameworkUpdate" /> 
    </Window.CommandBindings>

    <Window.InputBindings>
        <KeyBinding Command="ApplicationCommands.New" Key="N" Modifiers="Control"/>
        <KeyBinding Command="ApplicationCommands.Open" Key="O" Modifiers="Control"/>
        <KeyBinding Command="ApplicationCommands.Save" Key="S" Modifiers="Control"/>
        <KeyBinding Command="ApplicationCommands.SaveAs" Key="E" Modifiers="Control"/>
        <KeyBinding Command="ApplicationCommands.Find" Key="F" Modifiers="Control"/>
        <KeyBinding Command="ApplicationCommands.Delete" Key="X" Modifiers="Control"/>

        <KeyBinding Command="{x:Static anvil:MainWindow.EnterEditModeCommand}" Key="F1" />
        <KeyBinding Command="{x:Static anvil:MainWindow.EnterCreateModeCommand}" Key="F2" />
        <KeyBinding Command="{x:Static anvil:MainWindow.EnterZoomModeCommand}" Key="F3" />
        <KeyBinding Command="{x:Static anvil:MainWindow.EnterRespawnModeCommand}" Key="R" Modifiers="Control"/>
        <KeyBinding Command="{x:Static anvil:MainWindow.EnterAmbientModeCommand}" Key="M" Modifiers="Control"/>
        <KeyBinding Command="{x:Static anvil:MainWindow.ShowSQMEditorCommand}" Key="Q" Modifiers="Control"/>
        <KeyBinding Command="{x:Static anvil:MainWindow.RefreshMissionFromSqmCommand}" Key="U" Modifiers="Control"/>
    </Window.InputBindings>

    <Grid Margin="0" x:Name="MainGrid">
        <DockPanel>
            <Grid DockPanel.Dock="Top" Panel.ZIndex="10">
                <Grid.RowDefinitions>
                    <RowDefinition Height="25"/>
                    <RowDefinition Height="25"/>
                </Grid.RowDefinitions>

                <Menu Grid.Row="0">
                    <MenuItem Header="_File">
                        <MenuItem Header="_New" InputGestureText="Ctrl+N" Command="ApplicationCommands.New"/>
                        <MenuItem Header="_Open" InputGestureText="Ctrl+O" Command="ApplicationCommands.Open"/>
                        <MenuItem Header="_Save" InputGestureText="Ctrl+S" Command="ApplicationCommands.Save"/>
                        <MenuItem x:Name="RecentItemsMenu" Header="Open _Recent"></MenuItem>
                        <Separator></Separator>

                        <MenuItem Header="_Check for Framework Updates" IsEnabled="True" Command="{x:Static anvil:MainWindow.CheckForUpdatesCommand}" />
                        <MenuItem Header="_Apply Manual Framework Update" IsEnabled="True" Command="{x:Static anvil:MainWindow.ManualFrameworkUpdateCommand}" />
                        
                        <Separator></Separator>
                        
                        <MenuItem Header="E_xit" InputGestureText="Alt+F4" Click="ExitApplication"/>
                    </MenuItem>

                    <MenuItem Header="_Edit">

                        <MenuItem Header="S_QM Browser" InputGestureText="Ctrl+Q" Command="{x:Static anvil:MainWindow.ShowSQMEditorCommand}" />
                        <MenuItem Header="_Update from SQM" InputGestureText="Ctrl+U" Command="{x:Static anvil:MainWindow.RefreshMissionFromSqmCommand}" />
                        <Separator></Separator>

                        <MenuItem Header="_Edit Mode" x:Name="EditModeMenuItem" IsChecked="True" InputGestureText="F1" Command="{x:Static anvil:MainWindow.EnterEditModeCommand}"/>
                        <MenuItem Header="_Create Mode" x:Name="CreateModeMenuItem" InputGestureText="F2" Command="{x:Static anvil:MainWindow.EnterCreateModeCommand}"/>
                        <MenuItem Header="_Zoom Mode" x:Name="ZoomModeMenuItem" InputGestureText="F3" Command="{x:Static anvil:MainWindow.EnterZoomModeCommand}"/>
                        <Separator></Separator>
                        
                        <MenuItem Header="_Place Objective" InputGestureText="F" Command="{x:Static anvil:MainWindow.EnterCreateModeCommand}" />
                        <MenuItem Header="_Place Ambient" InputGestureText="Ctrl+M" Command="{x:Static anvil:MainWindow.EnterAmbientModeCommand}" />
                        <MenuItem Header="_Place Respawn" InputGestureText="Ctrl+R" Command="{x:Static anvil:MainWindow.EnterRespawnModeCommand}" />
                        <Separator></Separator>
                        
                        <MenuItem Header="_Find Objective" InputGestureText="Ctrl+F" Command="ApplicationCommands.Find"/>
                        <MenuItem Header="_Delete Objective" InputGestureText="Ctrl+X" Command="ApplicationCommands.Delete"/>
                    </MenuItem>

                    <MenuItem Header="_Generate">
                        <MenuItem Header="_Preview" Click="PreviewMissionInputs"/>
                        <MenuItem Header="_Export" InputGestureText="Ctrl+E" Command="ApplicationCommands.SaveAs" />
                        <MenuItem Header="_Clean and Export" Command="{x:Static anvil:MainWindow.PerformCleanBuildCommand}" />
                    </MenuItem>

                    <MenuItem Header="_Help">
                        <MenuItem Header="View _youtube Tutorial" IsEnabled="False" />
                        <MenuItem Header="_Visit Help Site" IsEnabled="False" />
                    </MenuItem>
                </Menu>

                <ToolBarPanel Grid.Row="1" Margin="0" Background="White">
                    <ToolBar Background="White" FontSize="14" FontFamily="Consolas">
                        <Button Content="New" Command="ApplicationCommands.New"/>
                        <Button Content="Open" Command="ApplicationCommands.Open"/>
                        <Button Content="Save" Command="ApplicationCommands.Save"/>
                        <Separator></Separator>
                        <Button Content="Preview" Command="{x:Static anvil:MainWindow.ShowSQMEditorCommand}"/>
                        <Button Content="Export" Command="ApplicationCommands.SaveAs" />
                    </ToolBar>
                </ToolBarPanel>
            </Grid>

            <Grid DockPanel.Dock="Right" Width="350" Panel.ZIndex="9">
                <Grid.RowDefinitions>
                    <RowDefinition Height="75*"/>
                    <RowDefinition Height="25*"/>
                </Grid.RowDefinitions>
                <xctk:PropertyGrid x:Name="ObjectiveProperties" Margin="5,5,5,0" Grid.Row="0" SelectedPropertyItemChanged="ObjectivePropertiesSelectedPropertyItemChanged" />
                <ListBox x:Name="ScriptSelector" Margin="5,0,5,5" Grid.Row="1" Background="White" SelectionMode="Multiple" />
            </Grid>
            
            <Canvas DockPanel.Dock="Left" x:Name="ObjectiveCanvas" Width="600" Height="600" MouseDown="CanvasMouseDown" HorizontalAlignment="Left" VerticalAlignment="Top" Margin="0,1,0,0" MouseUp="ObjectiveCanvasMouseButtonUp" Cursor="Hand" MouseMove="ObjectiveCanvas_MouseMove" MouseWheel="ObjectiveCanvasMouseWheel">
                <Canvas.RenderTransform>
                    <ScaleTransform x:Name="MapScale"></ScaleTransform>
                </Canvas.RenderTransform>
            </Canvas>
        </DockPanel>
        
        <StatusBar Height="25" Panel.ZIndex="5" VerticalAlignment="Bottom" >
            <StatusBarItem>
                <TextBlock x:Name="StatusLabel" />
            </StatusBarItem>
            <StatusBarItem HorizontalAlignment="Right">
                <Button x:Name="MissionLintButton" Content="Mission Appears Valid" Background="White" FontWeight="Bold" Foreground="Black" Click="MissionLintButtonClick" />
            </StatusBarItem>
        </StatusBar>
        
    </Grid>
</Window>
